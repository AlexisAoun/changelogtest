## 1.0.1 (2021-08-10)

### Test (1 change)

- [This is a test](AlexisAoun/changelogtest@6f62cc54b07c6bb72529b3db2011b18b030e6991)

## 1.0.0 (2021-08-09)

### Refactor (1 change)

- [Cleaned testing files](AlexisAoun/changelogtest@6890cfeb04bc23e1ce14292b3513cc5a9c46bd26)

## 0.0.6 (2021-08-09)

### Features (2 changes)

- [Add new script to handle api](AlexisAoun/changelogtest@6950e40a1668540d7983e64e47186214fa91920c)
- [Add new script for changelog entries](AlexisAoun/changelogtest@13c8ca885695052a0afd0e74a3e96d298b67ee76) ([merge request](AlexisAoun/changelogtest!3))
