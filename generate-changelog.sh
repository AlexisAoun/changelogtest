#!/usr/bin/env bash

set -euo pipefail

VERSION=$(curl -Ss --fail --request GET --header "PRIVATE-TOKEN: $1" "https://gitlab.com/api/v4/projects/$2/repository/tags" | jq -r '.[0] | .name')

curl -Ss --fail --header "PRIVATE-TOKEN: $1" --data "version=$VERSION" "https://gitlab.com/api/v4/projects/$2/repository/changelog"


