#!/usr/bin/env bash

set -euo pipefail

category=""
message=""

changelogCommit() {
  commitMsg=$(printf "$message \n\nChangelog: $category")
  git commit --allow-empty -m "$commitMsg" 
}

for i in "$@"; do
   case $i in
       -h|-\?|--help)
           echo "test"  
           exit
           ;;
       -c=*|--category=*)
           category="${i#*=}" 
           shift
           ;;
       -m=*|--message=*)
           message="${i#*=}" 
           shift
           ;;
       -?*)
           printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
           ;;
       *)               # Default case: No more options, so break out of the loop.
           break
   esac
done

if [[ $category && $message ]]; then
  changelogCommit
else 
  echo "Missing message and/or category"
fi

